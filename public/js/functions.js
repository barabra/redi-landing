/**
 * Created by jaska on 8.2.2017.
 */

var texts = [];
texts.push({"pos": {"left": 37, "top": 13}, "color":"#f36f35", "t":"Metro kuljettaa REDIn läpi noin 100 miljoonaa asiakasta vuodessa, ja metro pysähtyy kauppakeskuksen 3. kerroksessa 500 kertaa päivässä."});
texts.push({"pos": {"right": 7, "top": 10}, "color":"#9bb1a9", "t": "REDIn jo rakennettu ympäristö on Helsingin tiheimmin asuttu asuinalue. Uusi ja trendikäs Kalasatama on Suomen suurin kaupunkirakentamishanke."});
texts.push({"pos": {"left": 17, "top": 22}, "color":"#f1675e", "t":"Tästä kerroksesta pääsee myös suoraan lentämään."});
texts.push({"pos": {"left": 14, "top": 34}, "color":"#faaf40", "t":"Ensimmäisenä kokonaisena toimintavuotena kauppakeskukseen odotetaan 12 miljoonaa kävijää."});
texts.push({"pos": {"right": 14, "top": 55}, "color":"#103d9b", "t":"2 000 parkkipaikkaa autoille ja 3 000 paikkaa polkupyörille."});
texts.push({"pos": {"right": 23, "top": 63}, "color":"#4a3182", "t":"Lounaalla saatat kuulla veden yli leijonankarjahduksen."});
texts.push({"pos": {"left": 33, "top": 70}, "color":"#8dc53e", "t":"Tätä seinää pitkin kulkee kiipeilijöiden reitti."});
texts.push({"pos": {"right": 30, "top": 66}, "color":"#e23648", "t":"REDIin valmistuu kahdeksan tornitaloa. Kuudessa tornitalossa tulee asumaan yli 2 000 asukasta, jotka pääsevät hissillä suoraan kauppakeskukseen. Muut tornit ovat hotelli- ja toimistotorni."});
texts.push({"pos": {"left": 4, "bottom": 10}, "color":"#28ac8e", "t":"30 minuutin ajomatkan päässä REDIstä asuu 1,1 miljoonaa ihmistä."});
texts.push({"pos": {"left": 41, "bottom": 9}, "color":"#338b85", "t":"Keskimäärin 100 000 autoa ajaa REDIn läpi vuorokaudessa."});

function googleEvent(page) {
    dataLayer.push({
      'event': 'pageview',
      'virtualUrl': '/' + page 
    });
}

function sendForm(){
    var data = {};
    data.firstname = $("#form_firstname").val();
    data.lastname = $("#form_lastname").val();
    data.phone = $("#form_phone").val();
    data.email = $("#form_email").val();
    data.company = $("#form_company").val();
    data.message = $("#form_message").val();
    data.lottery = document.getElementById('form_lottery').checked ? 'yes' : 'no';

    var requiredFields = ['form_message', 'form_company', 'form_phone', 'form_email', 'form_lastname', 'form_firstname'];
    var valid = true;
    requiredFields.forEach(function(field){
        var el = $('#' + field);

        if(!el.val()){
            el.parent().find('.msg').html('Kenttä on pakollinen.');
            valid = false;
        }
        else{
            el.parent().find('.msg').html('');
        }
    });

    if(!valid){
        return false;
    }

    $.ajax({
        url: "php/save.php",
        type: 'POST',
        data: data
    }).always(function(data){
        //console.log("response", response);
        if(data.result == 1){
            $('#thanks').show();
            $('#form').hide();
            googleEvent("lomake-lahetetty");
        }else{
            alert("Tietojen vastaanotossa tapahtui virhe!");
        }
    });
}

var lastButtonClicked = 0;
function floorButtonClick(num){
    var info = $("#floorButtonInfo");
    if(num == lastButtonClicked){
        lastButtonClicked = 0;
        info.css("display", "none");
        return false;
    }else{
        lastButtonClicked = num;
    }

    var data = texts[num-1];

    info.css("backgroundColor", data.color);
    info.text(data.t);

    var p = data.pos;
    var p1 = {};
    if(p.left) p1.left = p.left+"%";
    if(p.right) p1.right = p.right+"%";
    if(p.top) p1.top = p.top+"%";
    if(p.bottom) p1.bottom = p.bottom+"%";

    if($('#floor').width() <= 500){
        p1 = {"left": "25%", "top": "35%"};
    }

    info.css({"left": "initial", "right": "initial", "top": "initial", "bottom": "initial"});
    info.css(p1);
    info.css("display", "block");

}

var $window = $(window);

function resize(){

    var width = $window.width();
    var height = $window.height();

    if( $("#floorsizer").css("display") == "block" ){
        $('#floor').detach().appendTo('#mobile-form-container');
        $('.floor-col').hide();
        $('#mobile-form-container').show();
    } else {
        $('#floor').detach().appendTo('.floor-col');
        $('.floor-col').show();
        $('#mobile-form-container').hide();        
    }

    if($("#floorsizer").css("display") == "none"){
        width = $('.floor-col').width();
    }

    var size = height < width ? height : width;
    if(size > 1040){
        size = 1040;
    }

    /*
    var width = $window.width();
    if(width < 1024){
        $('#floor').detach().appendTo('#mobile-form-container');
        $('.floor-col').hide();
        $('#mobile-form-container').show();
    }
    else{
        $('#floor').detach().appendTo('.floor-col');
        $('.floor-col').show();
        $('#mobile-form-container').hide();        
    }

    var height = $window.height();

    if(width >= 1024){
        width = $('.floor-col').width();
    }

    var size = height < width ? height : width;
    if(size > 1040){
        size = 1040;
    }
    */

    $('#floor').width(size - 30).height(size - 30); 
}