<?php
/**
 * Created by PhpStorm.
 * User: jaska
 * Date: 8.2.2017
 * Time: 18:38
 */


$data = $_POST;
$data["datetime"] = date("d.m.Y G:i:s");
$row = json_encode($data, JSON_UNESCAPED_UNICODE);
file_put_contents("formsubmits.txt", PHP_EOL . $row .",", FILE_APPEND);

header("Content-type: text/json");

$result = array("result" => 1);

echo json_encode($result);