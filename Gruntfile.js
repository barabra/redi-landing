module.exports = function(grunt) {

    'use strict';

    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        path: {
            bower: 'bower_components',
            wp: 'public'
        },
        
        autoprefixer: {
            options: {
                // Task-specific options go here.
            },
            dist: {
                expand: true,
                flatten: true,
                src: '<%= path.wp %>/build/*.css', // -> src/css/file1.css, src/css/file2.css
                dest: '<%= path.wp %>/build/' // -> dest/css/file1.css, dest/css/file2.css
            }

        },

        csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            src: '<%= path.wp %>/build/*.css'
        },

        cssmin: {
            dist: {
                expand: true,
                cwd: '<%= path.wp %>/build/',
                src: ['*.css'],
                dest: '<%= path.wp %>/build/',
                ext: '.css'
            }
        },

        sass: {
            options: {
                includePaths: ['<%= path.foundationscss %>', '<%= path.motionui %>']
            },
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: true                
                },
                files: {
                    '<%= path.wp %>/build/app.css': '<%= path.wp %>/scss/app.scss'
                }
            },
            test: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: true                
                },
                files: {
                    '<%= path.wp %>/build/app.css': '<%= path.wp %>/scss/app.scss'
                }
            }
        },

        concurrent: {
            tasks: ['buildcss'],
            options: {
                logConcurrentOutput: true
            }
        },

        watch: {

            sass: {
                files: '<%= path.wp %>/scss/**/*.scss',
                tasks: ['buildcss']
            }


        }
    });

    //Load NPM tasks
    require('load-grunt-tasks')(grunt);

    grunt.registerTask('buildcss', ['sass', 'autoprefixer', 'cssmin']);
    grunt.registerTask('build', ['concurrent']);
    grunt.registerTask('default', ['build', 'watch']);
    

};